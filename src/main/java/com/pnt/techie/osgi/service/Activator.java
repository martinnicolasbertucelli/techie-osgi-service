package com.pnt.techie.osgi.service;

import com.pnt.techie.osgi.service.definition.HelloService;
import com.pnt.techie.osgi.service.impl.HelloServiceImpl;
import java.util.Hashtable;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator {

    private ServiceReference<HelloService> reference;
    private ServiceRegistration<HelloService> registration;

    public void start(BundleContext context) throws Exception {
        registration = context.registerService(
                HelloService.class,
                new HelloServiceImpl(),
                new Hashtable<String, String>());
        reference = registration.getReference();
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
